<?php

namespace FizzBuzz;

class Rule implements RuleInterface
{
    /**
     * @var int
     */
    private $number;

    /**
     * @var string
     */
    private $text;

    /**
     * @param int $number
     * @param string $text
     */
    public function __construct($number, $text)
    {
        $this->number = $number;
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param int $number
     *
     * @return bool
     */
    public function match($number)
    {
        return $number % $this->number === 0;
    }
}
