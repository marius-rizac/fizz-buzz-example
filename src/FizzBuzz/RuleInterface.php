<?php

namespace FizzBuzz;

interface RuleInterface
{
    /**
     * @return string
     */
    public function getText();

    /**
     * @param int $number
     *
     * @return bool
     */
    public function match($number);
}
