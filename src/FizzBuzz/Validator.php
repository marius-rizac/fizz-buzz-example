<?php

namespace FizzBuzz;

class Validator
{
    /**
     * @var \FizzBuzz\RuleInterface[]
     */
    private $rules = [];

    /**
     * @param \FizzBuzz\RuleInterface $rule
     */
    public function addRule(RuleInterface $rule)
    {
        $this->rules[] = $rule;
    }

    /**
     * @param int $number
     *
     * @return int|string
     */
    public function check($number)
    {
        foreach ($this->rules as $rule) {
            if ($rule->match($number)) {
                return $rule->getText();
            }
        }

        return $number;
    }
}
