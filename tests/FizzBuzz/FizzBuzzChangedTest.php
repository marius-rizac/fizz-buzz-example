<?php

namespace tests\FizzBuzz;

use FizzBuzz\Rule;
use FizzBuzz\Validator;
use PHPUnit\Framework\TestCase;

class FizzBuzzChangedTest extends TestCase
{
    /**
     * @var \FizzBuzz\Validator
     */
    protected $validator;

    public function setUp()
    {
        $this->validator = new Validator();
        $this->validator->addRule(new Rule(21, 'FizzBuzz'));
        $this->validator->addRule(new Rule(3, 'Fizz'));
        $this->validator->addRule(new Rule(7, 'Buzz'));
    }

    /**
     * @dataProvider provideFizzNumbers
     *
     * @param int $number
     */
    public function testFizzOutput($number)
    {
        self::assertSame('Fizz', $this->validator->check($number));
    }

    /**
     * @dataProvider provideBuzzNumbers
     *
     * @param int $number
     */
    public function testBuzzOutput($number)
    {
        self::assertSame('Buzz', $this->validator->check($number));
    }

    /**
     * @dataProvider provideFizzBuzzNumbers
     *
     * @param int $number
     */
    public function testFizzBuzzOutput($number)
    {
        self::assertSame('FizzBuzz', $this->validator->check($number));
    }

    /**
     * @dataProvider provideNumbers
     *
     * @param int $number
     */
    public function testNumberOutput($number)
    {
        self::assertEquals($number, $this->validator->check($number));
    }

    /**
     * @return array
     */
    public function provideFizzNumbers()
    {
        return [
            [3],
            [6],
            [9],
        ];
    }

    /**
     * @return array
     */
    public function provideBuzzNumbers()
    {
        return [
            [7],
            [14],
            [35],
        ];
    }

    /**
     * @return array
     */
    public function provideFizzBuzzNumbers()
    {
        return [
            [21],
            [42],
            [84],
        ];
    }

    /**
     * @return array
     */
    public function provideNumbers()
    {
        return [
            [1],
            [2],
            [11],
            [13],
            [16],
        ];
    }
}
