.PHONY: test install deps

install:
	composer install -n

test:
	vendor/bin/phpunit

deps:
#	bin/phpsda analyze ./dependency.yml
	vendor/bin/phpda analyze ./dependency.yml
