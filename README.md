# Fizz-Buzz
Fizz-Buzz Example. Keep the library abstract and add the implementation in project level

## Installation

#### Git clone
```
git clone https://gitlab.com/marius-rizac/fizz-buzz-example.git
```

#### composer
```
{
  "minimum-stability": "dev",
  "prefer-stable": true,
  "require": {
    "marius-rizac/fizz-buzz-example": "dev-master"
  },
  "repositories": [
    {
      "type": "vcs",
      "url": "https://gitlab.com/marius-rizac/fizz-buzz-example.git"
    }
  ]
}
```

### Usage
```
<?php

require '/path/to/vendor/autoload.php';

$v1 = new \Demo\Validator();
$v1->addRule(new \Demo\Rule(15, 'FizzBuzz'));
$v1->addRule(new \Demo\Rule(3, 'Fizz'));
$v1->addRule(new \Demo\Rule(5, 'Buzz'));

for ($i=1; $i<=50; $i++) {
    echo $v1->check($i) . "\n";
}
```

#### You can have a different implementation with different data

```
<?php

require '/path/to/vendor/autoload.php';

$v2 = new \Demo\Validator();
$v2->addRule(new \Demo\Rule(28, 'AbstractProgramming'));
$v2->addRule(new \Demo\Rule(4, 'Abstract'));
$v2->addRule(new \Demo\Rule(7, 'Programming'));

for ($i=1; $i<=100; $i++) {
    echo $v2->check($i) . "\n";
}
```

### Generate Dependency Report
```
php bin/phpsda analyze ./dependency.yml
```
